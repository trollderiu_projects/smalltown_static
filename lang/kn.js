$.extend(window.lang, {

"startingConnection": "ಸಂಪರ್ಕವನ್ನು ಪ್ರಾರಂಭಿಸಿ ..",
"joiningGame": "ಆಟದ ಸೇರುವ ...",
"GameList": "ಗೇಮ್ ಪಟ್ಟಿ:",
"noMoreGames": "ಯಾವುದೇ ಆಟಗಳು ಇಲ್ಲ",
"emptyChat": "ಚಾಟ್ನಲ್ಲಿ ಏನಾದರೂ ಬರೆಯುವಲ್ಲಿ ಮೊದಲಿಗರಾಗಿರಿ!",
"createGame": "ಆಟ ರಚಿಸಿ",
"gameName": "ಆಟದ ಹೆಸರು",
"hideGameSwipe": "ಈ ಆಟವನ್ನು ಮರೆಮಾಡಲು ಸ್ವೈಪ್ ಮಾಡಿ ..",
"exitGameSwipe": "ನೀವು ಈ ಆಟದಿಂದ ನಿರ್ಗಮಿಸುತ್ತೀರಿ ..",
"removeGameSwipe": "ಆಟವನ್ನು ಶಾಶ್ವತವಾಗಿ ತೆಗೆದುಹಾಕಲು ಸ್ವೈಪ್ ಮಾಡಿ ..",
"playingHere": "ನೀವು ಇಲ್ಲಿ ಆಡುತ್ತಿದ್ದಾರೆ",
"playersIn": " ಸಕ್ರಿಯ ಆಟಗಾರರು",
"games": " ಆಟಗಳು.",
"Admin": "ನಿರ್ವಹಣೆ",
"adminHelp": "ನಿರ್ವಹಣೆ ಕಾರ್ಯಗಳು",
"NewCards": "ಹೊಸ ಕಾರ್ಡ್ಗಳು",
"newCardsHelp": "(ತನ್ನ ಇಸ್ಪೀಟೆಲೆಗಳನ್ನು ನೋಡಲು ಎಲ್ಲರಿಗೂ ತಿಳಿಸಿ)",
"StartGame": "ಆಟವನ್ನು ಪ್ರಾರಂಭಿಸಿ",
"startGameHelp": "(ಎಲ್ಲರೂ ನಿಕಟ ಕಣ್ಣುಗಳನ್ನು ಹೇಳಿ)",
"gameHelp": "ಆಟದ ನಿರ್ವಾಹಕ ಆಯ್ಕೆಗಳು",
"EndTurn": "ಟರ್ನ್ ಎಂಡ್",
"endTurnHelp": "ಎಂಡ್ ಡೇ ಟರ್ನ್",
"Game": "ಗೇಮ್",
"Password": "ಪಾಸ್ವರ್ಡ್",
"GamePublic": "ಗೇಮ್ ಸಾರ್ವಜನಿಕವಾಗಿದೆ",
"DayTime": "ಆಟಗಾರನ ದಿನ ಸಮಯ",
"OpenVoting": "ಆಟಗಾರರು ದಿನದಲ್ಲಿ ಮತ ಚಲಾಯಿಸಲಿ",
"AdminEndTurn": "ನಿರ್ವಹಣೆ ದಿನಾಚರಣೆಯ ಅಂತ್ಯವನ್ನು ಕೊನೆಗೊಳಿಸಬಹುದು",
"sec_p": "ಸೆಕೆಂಡ್ / ಪು",
"PlayingCards": "ಪ್ಲೇಯಿಂಗ್ ಕಾರ್ಡ್ಗಳು",
"cardsHelp": "ಕಾರ್ಡ್ ಪಟ್ಟಿ",
"Name": "ಹೆಸರು",
"UserSettings": "ಬಳಕೆದಾರ ಸೆಟ್ಟಿಂಗ್ಗಳು",
"userHelp": "ವೈಯಕ್ತಿಕ ಆಯ್ಕೆಗಳು",
"SpectatorMode": "ಸ್ಪೆಕ್ಟೇಟರ್ ಮೋಡ್",
"spectatorModeHelp": "ಆಡದೆ ಚಾಟ್ ಮಾಡಲು ಪ್ರೇಕ್ಷಕರಾಗಿರಿ",
"Image": "ಚಿತ್ರ",
"VibrationStrength": "ಕಂಪನ ಸಾಮರ್ಥ್ಯ",
"CleanErrors": "ಕ್ಲೀನ್ ದೋಷಗಳು",
"cleanHelp": "ಎನಾದರು ತೋಂದರೆ? 'smltown.tk' ನಲ್ಲಿ ನಮಗೆ ತಿಳಿಸಿ",
"Friends": "ಸ್ನೇಹಿತರು",
"friendsHelp": "ನಿಮ್ಮ ಸ್ನೇಹಿತರನ್ನು ಹುಡುಕಿ",
"Invite": "ಆಹ್ವಾನಿಸಿ",
"inviteHelp": "ಉಳಿಸಿದ ಸ್ನೇಹಿತರನ್ನು ಆಹ್ವಾನಿಸಿ",
"AddSocialId": "ಗುರುತಿಸುವಿಕೆಯನ್ನು ಸೇರಿಸಿ",
"EditSocialId": "ನಿಮ್ಮ ಗುರುತಿಸುವಿಕೆಯನ್ನು ಸಂಪಾದಿಸಿ",
"addSocialIdHelp": "ನಿಮ್ಮ ಗುರುತಿಸುವಿಕೆಯನ್ನು ಹುಡುಕಲು ಸಾಧ್ಯವಾಗುವಂತೆ ಸೇರಿಸಿ",
"Info": "ಮಾಹಿತಿ",
"infoHelp": "ಸಹಾಯ ಮತ್ತು ಆಟದ ಕೈಪಿಡಿ",
"CurrentURL": "ಪ್ರಸ್ತುತ URL",
"Notes": "ಟಿಪ್ಪಣಿಗಳು",
"infoNotes": "ಆಟದ ಮುಂದಿನ ತಿರುವುಗಳಿಗೆ ಟಿಪ್ಪಣಿಗಳನ್ನು ಉಳಿಸಿ",
"Back": "ಹಿಂತಿರುಗಿ",
"backHelp": "ಆಟದ ಪಟ್ಟಿಗೆ ಹಿಂತಿರುಗಿ",
"newCards": "ಹೊಸ ಪಾತ್ರ ಕಾರ್ಡ್ಗಳನ್ನು ಎದುರಿಸಲು",
"waitingNewGame": "ಹೊಸ ಆಟಕ್ಕಾಗಿ ಕಾಯುತ್ತಿದೆ ...",
"gameRestarted": "ಗೇಮ್ ಅನ್ನು ಮರುಪ್ರಾರಂಭಿಸಲಾಯಿತು",
"noName": "ಯಾವುದೇ ಹೆಸರು ಕಂಡುಬಂದಿಲ್ಲ",
"waitPlayersVotes": "ಕಾಯುವ ಆಟಗಾರರ ಮತಗಳು",
"nightTime": "ರಾತ್ರಿ ಸಮಯ",
"dayTime": "ದಿನ ಸಮಯ",
"wakingUp": "ಕಾಯುವ ಆಟಗಾರರು ಎಚ್ಚರಗೊಳ್ಳುತ್ತಾರೆ ..",
"waiting": "ಆಡಲು ಕಾಯುತ್ತಿದೆ",
"spectatorPlayer": "ಪ್ರೇಕ್ಷಕ",
"alive": "ಜೀವಂತವಾಗಿ",
"dead": "ಸತ್ತ",
"morePlayers": "ಹೆಚ್ಚಿನ ಆಟಗಾರರನ್ನು ಕರೆ ಮಾಡಿ",
"morePlayersHelp": "ಕನಿಷ್ಠ 4 ಆಟಗಾರರು ಆಡಲು",
"PRESELECT": "ಆಯ್ಕೆ ಮಾಡಲು ಮತ್ತೊಮ್ಮೆ ಕ್ಲಿಕ್ ಮಾಡಿ",
"Win": "ವಿನ್ನರ್ ತಂಡ",
"OK": "ಸರಿ",
"Cancel": "ರದ್ದುಮಾಡಿ",
"NoKills": "ಗ್ರಾಮವು ಒಂದು ಒಪ್ಪಂದವನ್ನು ಮಾಡಿಲ್ಲ ಮತ್ತು ಯಾರೂ ಬಂಧಿಸಲ್ಪಟ್ಟಿಲ್ಲ",
"NoKillsTonight": "ಪ್ರತಿಯೊಬ್ಬರೂ ಸಾವುಗಳಿಲ್ಲದೆಯೇ ಎಚ್ಚರಗೊಳ್ಳುತ್ತಾರೆ.",
"wasKilled": "ಸೆರೆಹಿಡಿಯಲಾಗಿದೆ",
"GettingDark": "ಡಾರ್ಕ್ ಪಡೆಯುತ್ತಿದೆ!",
"closeEyes": "ನಿನ್ನ ಕಣ್ಣುಗಳನ್ನು ಮುಚ್ಚಿಕೋ",
"wakeUp": "ಎದ್ದೇಳು",
"yourTurn": "ನಿಮ್ಮ ಸರದಿ",
"GoodMorning": "ಶುಭೋದಯ! ನಾವು ಯಾರನ್ನಾದರೂ ಲಿಂಚ್ ಮಾಡಬೇಕು ಎಂದು ಚರ್ಚಿಸಲು ಸಮಯ! <P> (ಸೂರ್ಯನು ಬಲಕ್ಕೆ ತಲುಪಿದಾಗ ಸಮಯವು ಮುಗಿಯುತ್ತದೆ) </ p>",
"gameWillStart": "ಗೇಮ್ ಶೀಘ್ರದಲ್ಲೇ ಪ್ರಾರಂಭವಾಗುತ್ತದೆ, ನಿಮ್ಮ ಕಣ್ಣು ಮುಚ್ಚಿ!",
"alive2": "ಕೇವಲ 2 ಆಟಗಾರರು ಮಾತ್ರ ಜೀವಂತರು. ದಿನದಲ್ಲಿ ಯಾವುದೇ ಕೊಲೆಗಳು ಸಂಭವಿಸುವುದಿಲ್ಲ",
"wasKilledTonight": "ಟುನೈಟ್ ನಿಧನರಾದರು",
"dayEnd": "ರಾತ್ರಿ ಬರುತ್ತಿದೆ. ಎಲ್ಲರೂ ನಿಮ್ಮ ಕಣ್ಣುಗಳನ್ನು ಮುಚ್ಚಿ.",
"GameOver": "ಗೇಮ್ ಓವರ್",
"botsAdded": "ಎನ್ಟೋಟ್ ಆಟಗಾರರು ಇಲ್ಲ, ಕೆಲವು ಬೋಟ್ಗಳನ್ನು ಸೇರಿಸಲಾಗಿದೆ. <P> ಆಟದ ಶೀಘ್ರದಲ್ಲೇ ಪ್ರಾರಂಭವಾಗುತ್ತದೆ </ p>",
"lynch": "ಹತ್ಯೆಗಾಗಿ ಒಬ್ಬ ಆಟಗಾರನನ್ನು ಆಯ್ಕೆ ಮಾಡಿ! <P> (ಸ್ಪೀಕ್ ಈಗ ನಿಷೇಧಿಸಲಾಗಿದೆ) </ p>",
"warnServer": "ಎಚ್ಚರಿಕೆ: ಸರ್ವರ್ ತುಂಬಾ ಸಮಯ ತೆಗೆದುಕೊಳ್ಳುತ್ತಿದೆ. ಇದು ಸರ್ವರ್ ದೋಷವಾಗಬಹುದು, ಸ್ವಲ್ಪ ನಿರೀಕ್ಷಿಸಿ ಅಥವಾ ಆಟವನ್ನು ರಿಫ್ರೆಶ್ ಮಾಡಬಹುದು.",
"openVotingEnabled": "ಮುಂದಿನ ಆಟಕ್ಕೆ, ದಿನದಲ್ಲಿ ಮತದಾನವನ್ನು ಅನುಮತಿಸಲಾಗುವುದು",
"openVotingDisabled": "ಮುಂದಿನ ಆಟಕ್ಕೆ, ದಿನದಲ್ಲಿ ಮತದಾನವನ್ನು ಅನುಮತಿಸಲಾಗುವುದಿಲ್ಲ",
"adminEndTurnEnabled": "ಮುಂದಿನ ಆಟಕ್ಕೆ, ಮಾಸ್ಟರ್ ದಿನ ಬದಲಾವಣೆಯನ್ನು ಕೊನೆಗೊಳಿಸಬಹುದು",
"adminEndTurnDisabled": "ಮುಂದಿನ ಆಟಕ್ಕೆ, ಮಾಸ್ಟರ್ ದಿನ ಬದಲಾವಣೆಯನ್ನು ಅಂತ್ಯಗೊಳಿಸುವುದಿಲ್ಲ",
"updatedDayTime": "ಮುಂದಿನ ಆಟಕ್ಕೆ, ದಿನದ ಅವಧಿಯನ್ನು ಬದಲಾಯಿಸಲಾಗಿದೆ",
"passwordRemoved": "ಪಾಸ್ವರ್ಡ್ ಆಟವನ್ನು ತೆಗೆದುಹಾಕಲಾಗಿದೆ",
"passwordUpdated": "ಅಪ್ಡೇಟ್ಗೊಳಿಸಲಾಗಿದೆ ಪಾಸ್ವರ್ಡ್ ಆಟ",
"adminRole": "ನೀವು ನಿರ್ವಾಹಕ ಪಾತ್ರವನ್ನು ಕದ್ದಿದ್ದೀರಿ!",
"noCard": "ಇನ್ನೂ ಯಾವುದೇ ಕಾರ್ಡ್ ಇಲ್ಲ",
"awakening": "ನಿರೀಕ್ಷಿಸಿ !, ಕೆಲವು ಆಟಗಾರರು ಎಚ್ಚರಗೊಳ್ಳುತ್ತಿದ್ದಾರೆ!",
"youSpectator": "ನೀವು ವೀಕ್ಷಕರಾಗಿದ್ದೀರಿ",
"youDead": "ಹೇ !, ನೀವು ಸತ್ತರು",
"somethingHappend": "ಯಾರಾದರೂ ಏನಾದರೂ ಯೋಜಿಸುತ್ತಿದ್ದಾರೆ ...",
"noCardsSelected": "ದಯವಿಟ್ಟು, ಇಸ್ಪೀಟೆಲೆಗಳನ್ನು ಆಯ್ಕೆ ಮಾಡಿ",
"duplicatedUserName": "ಹೆಸರು ಈಗಾಗಲೇ ಅಸ್ತಿತ್ವದಲ್ಲಿದೆ!",
"cannotCardsGame": "ಆಟದಲ್ಲಿ ಕಾರ್ಡ್ಗಳನ್ನು ಟಾಗಲ್ ಮಾಡಲು ಸಾಧ್ಯವಿಲ್ಲ",
"cannotCardsAdmin": "ಕೇವಲ ನಿರ್ವಹಣೆ ಆಟ ಕಾರ್ಡ್ಗಳನ್ನು ಟಾಗಲ್ ಮಾಡಬಹುದು",
"InviteFriends": "ಸ್ನೇಹಿತರನ್ನು ಆಹ್ವಾನಿಸಿ",
"Update": "ನವೀಕರಿಸಿ",
"addFriend": "ಗೆಳೆಯನನ್ನು ಸೇರಿಸು",
"SendInvitation": "ಆಹ್ವಾನ ಕಳುಹಿಸಿ",
"demoUser": "ಡೆಮೊ ಬಳಕೆದಾರ",
"winner": "ನೀವು ಆಟವನ್ನು ಗೆದ್ದಿದ್ದೀರಿ!",
"Share": "ಹಂಚಿಕೊಳ್ಳಿ!",
"ShareBy": "ಹಂಚಿಕೊಳ್ಳಿ",
"help_typeNameGame": "ಕನಿಷ್ಠ <b> 3 </ b> ಅಕ್ಷರಗಳ ಹೆಸರನ್ನು ಆಟದ ಹೆಸರನ್ನು ಟೈಪ್ ಮಾಡಿ ಮತ್ತು ಇನ್ನೂ ಅಸ್ತಿತ್ವದಲ್ಲಿಲ್ಲ.",
"help_createGame": "'ಗೇಮ್ ರಚಿಸಿ' ಕ್ಲಿಕ್ ಮಾಡಿ",
"error_tutorialPlayers": "ಆಟಗಾರರೊಂದಿಗೆ ಆಟದ ಕುರಿತು ಟ್ಯುಟೋರಿಯಲ್ ಅನ್ನು ಪ್ರಾರಂಭಿಸಲಾಗುವುದಿಲ್ಲ",
"reload": "ಮರುಲೋಡ್ ಮಾಡಿ",
"endingDay": "ಕೊನೆಗೊಳ್ಳುವ ದಿನ",
"nameMustLength": "ಹೆಸರು ಒಂದು ನಿಮಿಷದ 3 ಅಕ್ಷರಗಳ ಉದ್ದವನ್ನು ಹೊಂದಿರಬೇಕು",
"adminCanEndTurn": "ನಿರ್ವಹಣೆ ತಕ್ಷಣವೇ ಅಂತ್ಯಗೊಳ್ಳಬಹುದು",
"backAgain": "ನಿರ್ಗಮಿಸಲು ಮತ್ತೆ ಮತ್ತೆ",
"addUserInGame": "ಬಳಕೆದಾರರನ್ನು ಆಟದಲ್ಲಿ ಸೇರಿಸಿ",
"gamePublic": "ಸಾರ್ವಜನಿಕ ಆಟ",
"helpStop": "ಟ್ಯುಟೋರಿಯಲ್ ನಿಲ್ಲಿಸಲು",
"playGame": "ಆಟವಾಡು",
"setName": "ಸೆಟ್ ಹೆಸರು",
"savedName": "ಉಳಿಸಿದ ಹೆಸರು",
"cannotModifyCardsInGame": "ಆಟದಲ್ಲಿ ಕಾರ್ಡ್ಗಳನ್ನು ಮಾರ್ಪಡಿಸಲಾಗುವುದಿಲ್ಲ",
"gameNameExists": "ಆಟದ ಹೆಸರು ಅಸ್ತಿತ್ವದಲ್ಲಿದೆ",
"letVoteDay": "ದಿನದಲ್ಲಿ ಆಟಗಾರರು ಮತ ಚಲಾಯಿಸಲು ಅವಕಾಶ ಮಾಡಿಕೊಡಿ",
"timeByPlayer": "ಆಟಗಾರನು ದಿನದ ಸಮಯದ ಸೆಕೆಂಡುಗಳು",
"gameWithPassword": "ಪಾಸ್ವರ್ಡ್ನೊಂದಿಗೆ ಆಟ",
"tutorialQuestion": "ನೀವು ಟ್ಯುಟೋರಿಯಲ್ ಟೂರ್ನೊಂದಿಗೆ ಆಡಲು ಕಲಿಯಲು ಬಯಸುತ್ತೀರಾ?",
"FORUM": "ಫೋರಮ್",
"androidAppQuestion": "Android ಅಪ್ಲಿಕೇಶನ್ನೊಂದಿಗೆ ತೆರೆಯಿರಿ?",
"restartGameQuestion": "ಮರುಪ್ರಾರಂಭಿಸುವ ಆಟ?",
"playGameQuestion": "ಈ ಆಟಕ್ಕೆ ಸೇರಲು ನೀವು ಬಯಸುವಿರಾ? <br/> (ನೀವು ಈಗ ವೀಕ್ಷಕರಾಗಿದ್ದೀರಿ)",
"restartGame": "ಮರುಪ್ರಾರಂಭಿಸಿ ಆಟ",
"startGame": "ಆಟವನ್ನು ಪ್ರಾರಂಭಿಸಿ",
"nightSelect": "ರಾತ್ರಿ ಆಯ್ಕೆ",
"cannotEmpty": "ಖಾಲಿಯಾಗಿಲ್ಲ!",
"tutorialDone": "ಟ್ಯುಟೋರಿಯಲ್ ಮಾಡಲಾಗುತ್ತದೆ",
"gameNameAlreadyExists": "ಆಟದ ಹೆಸರು ಈಗಾಗಲೇ ಅಸ್ತಿತ್ವದಲ್ಲಿದೆ",
"spectatorMode": "ಸೇರಲು ಕ್ಲಿಕ್ ಮಾಡಿ (ಪ್ರೇಕ್ಷಕ)",
"gameNotPublic": "ಆಟವು ಸಾರ್ವಜನಿಕವಲ್ಲ",
"isBot": "ಬೋಟ್ ಆಗಿದೆ",
"friendAdded": "ಸ್ನೇಹಿತ ಸೇರಿಸಲಾಗಿದೆ",
"OrCopyLink": ".. ಅಥವಾ ನಕಲಿಸಿ ಈ ಲಿಂಕ್ ಹಂಚಿಕೊಳ್ಳಿ:",
"cantPlaySpectator": "ಈಗಾಗಲೇ ಪ್ರಾರಂಭವಾದಲ್ಲಿ ನೀವು ಆಟಕ್ಕೆ ಸೇರಬಾರದು!",
"gameRestartForPlayersChange": "ಆಟಗಾರರ ಸಂಖ್ಯೆಯು ಬದಲಾಗಿರುವುದರಿಂದ ಗೇಮ್ ಅನ್ನು ಮರುಪ್ರಾರಂಭಿಸಲಾಗಿದೆ",
"someSpectator": "ಪ್ರಸ್ತುತ ವೀಕ್ಷಕರು ಆಟಗಾರರು ಆಟವನ್ನು ಆಡುವುದಿಲ್ಲ. <br> ನೀವು ಎಲ್ಲರೂ ಆಡಲು ಬಯಸಿದರೆ, ಅವರು ಸೇರಲು ತಮ್ಮ ಹೆಸರನ್ನು ಕ್ಲಿಕ್ ಮಾಡಬೇಕು!",
"clipboardCopy": "ಕ್ಲಿಪ್ಬೋರ್ಡ್ಗೆ ನಕಲಿಸಲಾಗಿದೆ",
"shareThisError": "ದಯವಿಟ್ಟು ಈ ದೋಷವನ್ನು 'smltown.tk' ವೆಬ್ಪುಟದಲ್ಲಿ ತಿಳಿಸಿ, ಧನ್ಯವಾದಗಳು.",
"lookingGames": "ಆಟಗಳು ಹುಡುಕುತ್ತಿರುವ ..",
});
