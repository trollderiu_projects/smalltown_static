$.extend(window.lang, {
    //INDEX ///////////////////////////////////////////////////////////////////
    //
    "startingConnection": "starting connection.."
    ,
    "joiningGame": "joining the game..."
    ,
    //GAME LIST TEXTS//////////////////////////////////////////////////////////
    "GameList": "Game List:"
    ,
    "noMoreGames": "no more games"
    ,
    "emptyChat": "Be the first to write something in the chat!"
    ,
    "createGame": "create game"
    ,
    "gameName": "game name"
    ,
    "hideGameSwipe": "Swipe to hide this game.. "
    ,
    "exitGameSwipe": "You will exit from this game.. "
    ,
    "removeGameSwipe": "Swipe to remove game permanently.. "
    ,
    "playingHere": "you are playing here"
    ,
    "playersIn": " active players in ",
    "games": " games."
    ,
    //GAME TEXTS//////////////////////////////////////////////////////////////
    //
    //MENU
    //'Administracion', en vez de 'Master' -> 'Dominar'
    "Admin": "Admin"
    ,
    "adminHelp": "admin actions"
    ,
    "NewCards": "New cards"
    ,
    "newCardsHelp": "(tell everyone to look his playing cards)"
    ,
    "StartGame": "Start game"
    ,
    "startGameHelp": "(say everyone close eyes)"
    ,
    "gameHelp": "game ADMIN options"
    ,
    "EndTurn": "End turn"
    ,
    "endTurnHelp": "End day turn"
    ,
    //options
    "Game": "Game"
    ,
    "Password": "Password"
    ,
    "GamePublic": "Game is public"
    ,
    "DayTime": "Day time by player"
    ,
    "OpenVoting": "Let players vote during the day"
    ,
    "AdminEndTurn": "Admin can end the day shift"
    ,
    "sec_p": "sec/p"
    ,
    //cards
    "PlayingCards": "Playing Cards"
    ,
    "cardsHelp": "playing card list"
    ,
    //user
    "Name": "Name"
    ,
    "UserSettings": "User Settings"
    ,
    "userHelp": "personal options"
    ,
    "SpectatorMode": "Spectator mode"
    ,
    "spectatorModeHelp": "be spectator to chat without playing"
    ,
    "Image": "Image"
    ,
    "VibrationStrength": "Vibration Strength"
    ,
    "CleanErrors": "Clean Errors"
    ,
    "cleanHelp": "Any problem? let us know on 'smltown.tk'"
    ,
    //friends
    "Friends": "Friends"
    ,
    "friendsHelp": "find your friends"
    ,
    "Invite": "Invite"
    ,
    "inviteHelp": "invite saved friends"
    ,
    "AddSocialId": "Add identificator"
    ,
    "EditSocialId": "Edit your identificator"
    ,
    "addSocialIdHelp": "Add your ID to be found"
    ,
    //info
    "Info": "Info"
    ,
    "infoHelp": "help and game manual"
    ,
    "CurrentURL": "Current URL"
    ,
    //notes
    "Notes": "Notes"
    ,
    "infoNotes": "save your notes for the next shifts"
    ,
    //back
    "Back": "Return"
    ,
    "backHelp": "back to game list"
    ,
    //
    //HEAD STATUS GAME    
    "newCards": "deal new role cards"
    ,
    "waitingNewGame": "waiting for a new game..."
    ,
    "gameRestarted": "Game was restarted"
    ,
    "noName": "no name was found"
    ,
    "waitPlayersVotes": "waiting players votes"
    ,
    "nightTime": "night time"
    ,
    "dayTime": "day time"
    ,
    "wakingUp": "waiting players wake up.."
    ,
    //
    //PLAYER LIST
    "waiting": "waiting to play"
    ,
    "spectatorPlayer": "spectator"
    ,
    "alive": "alive"
    ,
    "dead": "dead"
    ,
    "morePlayers": "call more players"
    ,
    "morePlayersHelp": "minimum 4 players to play"
    ,
    "PRESELECT": "click again to select"
    ,
    "Win": "WINNER TEAM"
    ,
    //
    //MESSAGES ////////////////////////////////////////////////////////////
    //
    //NOTIFICATIONS
    "OK": "OK"
    ,
    "Cancel": "Cancel"
    ,
    "NoKills": "The village has not reached an agreement and no one has been lynched"
    ,
    "NoKillsTonight": "Everyone wakes up without deaths."
    ,
    "wasKilled": "has been lynched"
    ,
    "GettingDark": "Is getting dark!"
    ,
    "closeEyes": "close your eyes"
    ,
    "wakeUp": "wake up"
    ,
    "yourTurn": "is your turn"
    ,
    "GoodMorning": "Good Morning! It's time to discuss who we should lynch! <p>(When the sun reaches the far right, the time will be over)</p>"
    ,
    "gameWillStart": "Game will start soon, close your eyes!"
    ,
    "alive2": "Only 2 players are alive. No kills could happen at day"
    ,
    "wasKilledTonight": "has died tonight"
    ,
    "dayEnd": "Night is coming. Everybody close your eyes."
    ,
    "GameOver": "GAME OVER"
    ,
    "botsAdded": "There's not enought players, some bots have been added. <p>The game will start soon</p>"
    ,
    "lynch": "Select one player for the lynching! <p>(speak is now forbidden)</p>"
    ,
    //
    //FLASH MESSAGES
    //
    //server
    "warnServer": "warn: server is taking too long. It can be a server error, wait s little or refresh the game."
    ,
    //server events from menu
    "openVotingEnabled": "For the next game, voting will be allowed during the day"
    ,
    "openVotingDisabled": "For the next game, voting will not be allowed during the day"
    ,
    "adminEndTurnEnabled": "For the next game, the master can end the day shift"
    ,
    "adminEndTurnDisabled": "For the next game, the master can not end the day shift"
    ,
    "updatedDayTime": "For the next game, the duration of the day has been changed"
    ,
    "passwordRemoved": "password game was removed"
    ,
    "passwordUpdated": "updated password game"
    ,
    "adminRole": "you stole admin role!"
    ,
    //game events
    "noCard": "no card yet"
    ,
    //player list events    
    "awakening": "Wait!, some players are awakening!"
    ,
    "youSpectator": "you are a spectator"
    ,
    "youDead": "hey!, you are dead"
    ,
    "somethingHappend": "someone is plotting something..."
    ,
    //menu events
    "noCardsSelected": "please, select playing cards"
    ,
    "duplicatedUserName": "name already exists!"
    ,
    "cannotCardsGame": "cannot toggle cards in game"
    ,
    "cannotCardsAdmin": "only admin can toggle game cards"
    ,
    //
    //SOCIAL
    //invite 
    "InviteFriends": "Invite friends"
    ,
    "Update": "Update"
    ,
    "addFriend": "add friend"
    ,
    "SendInvitation": "Send Invitation"
    ,
    "demoUser": "demo user"
    ,
    //share
    "winner": "You won the game!"
    ,
    "Share": "Share!"
    ,
    "ShareBy": "Share by"
    ,
    //
    //HELP
    "help_typeNameGame": "Type name game whith a minimum of <b>3</b> characters and not exists yet."
    ,
    "help_createGame": "Click on 'create game'"
    ,
    //
    //ERRORS
    "error_tutorialPlayers": "Can't start tutorial on game with players"
            //
    ,
    "reload": "reload"
    ,
    "endingDay": "ending day"
    ,
    "nameMustLength": "name must have a mininmum 3 characters length"
    ,
    "adminCanEndTurn": "admin can end turn immediately"
    ,
    "backAgain": "back again to exit"
    ,
    "addUserInGame": "add user in game"
    ,
    "gamePublic": "public game"
    ,
    "helpStop": "stop tutorial"
    ,
    "playGame": "play game"
    ,
    "setName": "set name"
    ,
    "savedName": "saved name"
    ,
    "cannotModifyCardsInGame": "cannot modify cards in game"
    ,
    "gameNameExists": "game name exists"
    ,
    "letVoteDay": "let players vote during the day"
    ,
    "timeByPlayer": "seconds of day time by player"
    ,
    "gameWithPassword": "game with password"
    ,
    "tutorialQuestion": "Do you want to learn to play with a tutorial tour?"
    ,
    "FORUM": "FORUM"
    ,
    "androidAppQuestion": "open with android app?"
    ,
    "restartGameQuestion": "restart game?"
    ,
    "playGameQuestion": "Do you want to join this game? <br/> (you are now a spectator)"
    ,
    "restartGame": "restart game"
    ,
    "startGame": "start game"
    ,
    "nightSelect": "night select"
    ,
    "cannotEmpty": "cannot empty!"
    ,
    "tutorialDone": "tutorial done"
    ,
    "gameNameAlreadyExists": "game name already exists"
    ,
    "spectatorMode": "click to join (spectator)"
    ,
    "gameNotPublic": "game not public"
    ,
    "isBot": "is bot"
    ,
    "friendAdded": "friend added"
    ,
    "OrCopyLink": "..or copy and share this link: "
    ,
    "cantPlaySpectator": "You can't join to game if has already begun!"
    ,
    "gameRestartForPlayersChange": "Game has been restarted because players number has changed"
    ,
    "someSpectator": "Current spectators players won't play the game. <br> If you want them all to play, they must click on their name to join!"

    ,
    "clipboardCopy": "Copied to clipboard"
    ,
    "shareThisError": "Please let us know this error on 'smltown.tk' webpage, thanks."
    ,
    "lookingGames": "looking for games.."
});
