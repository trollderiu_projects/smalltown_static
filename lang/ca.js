$.extend(window.lang, {

"startingConnection": "Inici de la connexió ..",
"joiningGame": "Unint-se al joc ...",
"GameList": "Llista de jocs:",
"noMoreGames": "No més jocs",
"emptyChat": "Sigues el primer a escriure alguna cosa al xat!",
"createGame": "Crear joc",
"gameName": "Nom del joc",
"hideGameSwipe": "Fes lliscar el dit per ocultar aquest joc.",
"exitGameSwipe": "Sortiràs d'aquest joc ..",
"removeGameSwipe": "Feu lliscar el dit per eliminar el joc de manera permanent.",
"playingHere": "Estàs jugant aquí",
"playersIn": " Jugadors actius a",
"games": " jocs.",
"Admin": "Admin",
"adminHelp": "Accions d'administració",
"NewCards": "Targetes noves",
"newCardsHelp": "(Digues-li a tothom que miri les seves cartes)",
"StartGame": "Començar el joc",
"startGameHelp": "(Diguem a tots els ulls tancats)",
"gameHelp": "Opcions de joc ADMIN",
"EndTurn": "Fi al final",
"endTurnHelp": "Fi al dia de finalització",
"Game": "Joc",
"Password": "Contrasenya",
"GamePublic": "El joc és públic",
"DayTime": "Dia per jugador",
"OpenVoting": "Deixeu que els jugadors votin durant el dia",
"AdminEndTurn": "L'administrador pot finalitzar el canvi del dia",
"sec_p": "Sec / p",
"PlayingCards": "Jugant a les cartes",
"cardsHelp": "Llista de targetes",
"Name": "Nom",
"UserSettings": "Configuració de l'usuari",
"userHelp": "Opcions personals",
"SpectatorMode": "Mode d'espectador",
"spectatorModeHelp": "Ser espectador per xatejar sense jugar",
"Image": "Imatge",
"VibrationStrength": "Força de vibració",
"CleanErrors": "Errors nets",
"cleanHelp": "Algun problema? Avísenos a 'smltown.tk'",
"Friends": "Amics",
"friendsHelp": "Trobeu els vostres amics",
"Invite": "Convidar",
"inviteHelp": "Convidar amics guardats",
"AddSocialId": "Afegeix un identificador",
"EditSocialId": "Editeu el vostre identificador",
"addSocialIdHelp": "Afegiu el vostre identificador perquè es pugui trobar",
"Info": "Info",
"infoHelp": "Manual d'ajuda i joc",
"CurrentURL": "URL actual",
"Notes": "Notes",
"infoNotes": "Guardar notes als propers torns de joc",
"Back": "Retorn",
"backHelp": "Torna a la llista de jocs",
"newCards": "Tracteu noves targetes de rol",
"waitingNewGame": "Esperant un nou joc ...",
"gameRestarted": "S'ha reiniciat el joc",
"noName": "No s'ha trobat cap nom",
"waitPlayersVotes": "Votants esperant vots",
"nightTime": "El temps nocturn",
"dayTime": "Dia",
"wakingUp": "Els jugadors esperen desperten ..",
"waiting": "Esperant jugar",
"spectatorPlayer": "Espectador",
"alive": "Viu",
"dead": "Mort",
"morePlayers": "Truca a més jugadors",
"morePlayersHelp": "Mínim 4 jugadors a jugar",
"PRESELECT": "Feu clic de nou per seleccionar",
"Win": "EQUIP GUANYADOR",
"OK": "D'acord",
"Cancel": "Cancel · lar",
"NoKills": "El poble no ha arribat a un acord i ningú no ha estat lincat",
"NoKillsTonight": "Tothom es desperta sense morts.",
"wasKilled": "Ha estat linchado",
"GettingDark": "S'està fosc!",
"closeEyes": "tanca els ulls",
"wakeUp": "Desperta",
"yourTurn": "És el vostre torn",
"GoodMorning": "Bon dia! És el moment de parlar de qui hem de lligar! <P> (Quan el sol arribi a l'extrem dret, el temps acabarà) </ p>",
"gameWillStart": "El joc començarà aviat, tanca els teus ulls!",
"alive2": "Només 2 jugadors estan vius. Cap mata pot passar al dia",
"wasKilledTonight": "Ha mort aquesta nit",
"dayEnd": "La nit arriba. Tothom tanca els ulls.",
"GameOver": "FI DEL JOC",
"botsAdded": "No hi ha jugadors poc jugadors, s'han afegit alguns robots. <P> El joc començarà aviat </ p>",
"lynch": "Seleccioneu un jugador per al linxament. <P> (el discurs està ara prohibit) </ p>",
"warnServer": "Adverteix: el servidor triga massa. Pot ser un error del servidor, espereu poc o actualitzeu el joc.",
"openVotingEnabled": "Per al següent partit, la votació es permetrà durant el dia",
"openVotingDisabled": "Per al següent partit, la votació no es permetrà durant el dia",
"adminEndTurnEnabled": "Per al següent joc, el màster pot acabar el torn de dia",
"adminEndTurnDisabled": "Per al següent joc, el màster no pot finalitzar el canvi del dia",
"updatedDayTime": "Per al següent joc, la durada del dia s'ha canviat",
"passwordRemoved": "S'ha eliminat el joc de contrasenya",
"passwordUpdated": "Joc de contrasenyes actualitzat",
"adminRole": "Has robat el paper de l'administrador!",
"noCard": "Cap targeta encara",
"awakening": "Espera, alguns jugadors estan despertant!",
"youSpectator": "Ets un espectador",
"youDead": "Hey !, ets mort",
"somethingHappend": "Algú està traçant alguna cosa ...",
"noCardsSelected": "Si us plau, seleccioneu les cartes de joc",
"duplicatedUserName": "El nom ja existeix!",
"cannotCardsGame": "No es poden canviar les cartes al joc",
"cannotCardsAdmin": "Només l'administrador pot canviar les targetes de joc",
"InviteFriends": "Convidar amics",
"Update": "Actualització",
"addFriend": "afegir amic",
"SendInvitation": "Enviar invitació",
"demoUser": "Usuari demo",
"winner": "Has guanyat el joc!",
"Share": "Compartir!",
"ShareBy": "Comparteix-ho per",
"help_typeNameGame": "Escriu un joc de noms amb un mínim de <b> 3 </ b> i encara no existeix.",
"help_createGame": "Feu clic a 'Crear joc'",
"error_tutorialPlayers": "No es pot iniciar el tutorial en joc amb els jugadors",
"reload": "recarregar",
"endingDay": "Dia final",
"nameMustLength": "El nom ha de tenir una longitud mínima de 3 caràcters",
"adminCanEndTurn": "L'administrador pot acabar de girar immediatament",
"backAgain": "Torna a sortir",
"addUserInGame": "Afegiu usuari al joc",
"gamePublic": "Joc públic",
"helpStop": "Stop tutorial",
"playGame": "jugar a un joc",
"setName": "Estableix el nom",
"savedName": "Nom guardat",
"cannotModifyCardsInGame": "No es poden modificar les targetes al joc",
"gameNameExists": "El nom del joc existeix",
"letVoteDay": "Deixa que els jugadors votin durant el dia",
"timeByPlayer": "Segons del temps del dia pel jugador",
"gameWithPassword": "Joc amb contrasenya",
"tutorialQuestion": "Voleu aprendre a jugar amb una gira tutorial?",
"FORUM": "FÒRUM",
"androidAppQuestion": "Obre amb l'aplicació de Android?",
"restartGameQuestion": "Reinicia el joc?",
"playGameQuestion": "Voleu unir-vos a aquest joc? <br/> (ara ets un espectador)",
"restartGame": "Reiniciar el joc",
"startGame": "començar el joc",
"nightSelect": "Nit seleccionada",
"cannotEmpty": "No es pot buidar!",
"tutorialDone": "Tutorial fet",
"gameNameAlreadyExists": "El nom del joc ja existeix",
"spectatorMode": "Feu clic per unir-se (espectador)",
"gameNotPublic": "El joc no és públic",
"isBot": "És bot",
"friendAdded": "Amic afegit",
"OrCopyLink": ".. o copieu i compartiu aquest enllaç:",
"cantPlaySpectator": "No pots unir-te al joc si ja has començat!",
"gameRestartForPlayersChange": "S'ha reiniciat el joc perquè el número de jugadors ha canviat",
"someSpectator": "Els jugadors actuals dels espectadors no jugaran el joc. <br> Si vols que tothom jugi, han de fer clic al seu nom per unir-se!",
"clipboardCopy": "S'ha copiat al porta-retalls",
"shareThisError": "Si us plau, aviseu aquest error a la pàgina web 'smltown.tk', gràcies.",
"lookingGames": "A la recerca de jocs ..",
});
