$.extend(window.lang, {

"startingConnection": "بدء الاتصال ..",
"joiningGame": "الانضمام إلى اللعبة ...",
"GameList": "قائمة الألعاب:",
"noMoreGames": "لا مزيد من الألعاب",
"emptyChat": "كن أول من يكتب شيئا في الدردشة!",
"createGame": "ابتكر لعبة",
"gameName": "اسم اللعبة",
"hideGameSwipe": "مرر سريعا لإخفاء هذه اللعبة ..",
"exitGameSwipe": "سوف الخروج من هذه اللعبة ..",
"removeGameSwipe": "مرر سريعا لإزالة اللعبة نهائيا ..",
"playingHere": "كنت تلعب هنا",
"playersIn": " اللاعبين النشطين في",
"games": " ألعاب.",
"Admin": "مشرف",
"adminHelp": "إجراءات المشرف",
"NewCards": "بطاقات جديدة",
"newCardsHelp": "(أخبر الجميع أن ينظر له أوراق اللعب)",
"StartGame": "بدء اللعبة",
"startGameHelp": "(يقول الجميع عيون قريبة)",
"gameHelp": "لعبة خيارات الادارة",
"EndTurn": "نهاية بدوره",
"endTurnHelp": "نهاية اليوم",
"Game": "لعبه",
"Password": "كلمه السر",
"GamePublic": "لعبة عامة",
"DayTime": "يوم من قبل لاعب",
"OpenVoting": "السماح للاعبين التصويت خلال النهار",
"AdminEndTurn": "يمكن للمشرف إنهاء نوبة اليوم",
"sec_p": "ثانية / ص",
"PlayingCards": "لعب الورق",
"cardsHelp": "قائمة البطاقات",
"Name": "اسم",
"UserSettings": "إعدادات المستخدم",
"userHelp": "الخيارات الشخصية",
"SpectatorMode": "وضع المشاهد",
"spectatorModeHelp": "يكون المتفرج للدردشة دون اللعب",
"Image": "صورة",
"VibrationStrength": "قوة الاهتزاز",
"CleanErrors": "أخطاء نظيفة",
"cleanHelp": "اي مشكلة؟ واسمحوا لنا أن نعرف على 'smltown.tk'",
"Friends": "اصحاب",
"friendsHelp": "العثور على أصدقائك",
"Invite": "دعا",
"inviteHelp": "دعوة الأصدقاء المحفوظة",
"AddSocialId": "إضافة معرف",
"EditSocialId": "عدل معرفك",
"addSocialIdHelp": "إضافة إيدنتيفيكاتور الخاص بك لتصبح قابلة للبحث",
"Info": "معلومات",
"infoHelp": "مساعدة ودليل اللعبة",
"CurrentURL": "عنوان ورل الحالي",
"Notes": "ملاحظات",
"infoNotes": "حفظ الملاحظات على المنعطفات القادمة من اللعبة",
"Back": "إرجاع",
"backHelp": "العودة إلى قائمة اللعبة",
"newCards": "التعامل مع بطاقات الأدوار الجديدة",
"waitingNewGame": "في انتظار لعبة جديدة ...",
"gameRestarted": "تم إعادة تشغيل اللعبة",
"noName": "لم يتم العثور على اسم",
"waitPlayersVotes": "ينتظر اللاعبين الأصوات",
"nightTime": "وقت الليل",
"dayTime": "النهار",
"wakingUp": "في انتظار اللاعبين يستيقظون ..",
"waiting": "في انتظار للعب",
"spectatorPlayer": "المشاهد",
"alive": "على قيد الحياة",
"dead": "ميت",
"morePlayers": "استدعاء المزيد من اللاعبين",
"morePlayersHelp": "الحد الأدنى 4 لاعبين للعب",
"PRESELECT": "انقر مرة أخرى لتحديد",
"Win": "فريق الفائز",
"OK": "حسنا",
"Cancel": "إلغاء",
"NoKills": "ولم تتوصل القرية إلى اتفاق ولم يتم إعدام أحد",
"NoKillsTonight": "الجميع يستيقظ دون وفيات.",
"wasKilled": "قد تم إعدامه",
"GettingDark": "هو الحصول على الظلام!",
"closeEyes": "اغلق عينيك",
"wakeUp": "استيقظ",
"yourTurn": "دورك",
"GoodMorning": "صباح الخير! لقد حان الوقت لمناقشة من يجب أن نسق! <p> (عندما تصل الشمس إلى أقصى اليمين، فإن الوقت سيكون أكثر) </ p>",
"gameWillStart": "سوف تبدأ اللعبة قريبا، تغمض عينيك!",
"alive2": "فقط 2 لاعبين على قيد الحياة. لا يمكن أن يحدث قتل في اليوم",
"wasKilledTonight": "قد توفي الليلة",
"dayEnd": "ليلة قادمة. الجميع يغمض عينيك.",
"GameOver": "انتهت اللعبة",
"botsAdded": "ليس هناك لاعبين إنوت، تمت إضافة بعض السير. <p> ستبدأ اللعبة قريبا </ p>",
"lynch": "اختيار لاعب واحد للالقتل! <p> (الكلام محظور الآن) </ p>",
"warnServer": "تحذير: الخادم يستغرق وقتا طويلا. يمكن أن يكون خطأ الخادم، والانتظار قليلا أو تحديث اللعبة.",
"openVotingEnabled": "بالنسبة للمباراة التالية، سيسمح بالتصويت خلال النهار",
"openVotingDisabled": "في المباراة القادمة، لن يسمح التصويت خلال النهار",
"adminEndTurnEnabled": "للعبة القادمة، يمكن للسيد إنهاء نوبة اليوم",
"adminEndTurnDisabled": "للعبة القادمة، سيد لا يمكن إنهاء نوبة اليوم",
"updatedDayTime": "للعبة القادمة، تم تغيير مدة اليوم",
"passwordRemoved": "تمت إزالة لعبة كلمة المرور",
"passwordUpdated": "لعبة كلمة المرور المحدثة",
"adminRole": "سرقت دور المشرف!",
"noCard": "أي بطاقة حتى الآن",
"awakening": "انتظر !، بعض اللاعبين يستيقظون!",
"youSpectator": "كنت متفرجا",
"youDead": "مهلا !، أنت ميت",
"somethingHappend": "شخص ما يخطط لشيء ...",
"noCardsSelected": "الرجاء تحديد بطاقات اللعب",
"duplicatedUserName": "الاسم موجود من قبل!",
"cannotCardsGame": "لا يمكن تبديل بطاقات في اللعبة",
"cannotCardsAdmin": "المشرف فقط يمكن تبديل بطاقات اللعبة",
"InviteFriends": "ادعو أصدقاء",
"Update": "تحديث",
"addFriend": "إضافة صديق",
"SendInvitation": "إرسال دعوة",
"demoUser": "المستخدم التجريبي",
"winner": "لقد فزت في المباراة!",
"Share": "شارك!",
"ShareBy": "مشاركة بواسطة",
"help_typeNameGame": "اكتب اسم اللعبة مع حد أدنى من الأحرف <b> 3 </ b> وليس موجودا حتى الآن.",
"help_createGame": "انقر على 'إنشاء لعبة'",
"error_tutorialPlayers": "لا يمكن بدء البرنامج التعليمي على اللعبة مع اللاعبين",
"reload": "إعادة تحميل",
"endingDay": "نهاية اليوم",
"nameMustLength": "يجب أن يكون اسم مينينموم 3 أحرف طول",
"adminCanEndTurn": "المشرف يمكن أن ينتهي بدوره على الفور",
"backAgain": "مرة أخرى للخروج",
"addUserInGame": "إضافة المستخدم في اللعبة",
"gamePublic": "لعبة عامة",
"helpStop": "وقف البرنامج التعليمي",
"playGame": "ألعب لعبة",
"setName": "اسم مجموعة",
"savedName": "حفظ الاسم",
"cannotModifyCardsInGame": "لا يمكن تعديل بطاقات في اللعبة",
"gameNameExists": "اسم اللعبة موجود",
"letVoteDay": "السماح للاعبين التصويت خلال النهار",
"timeByPlayer": "ثانية من وقت اليوم من قبل لاعب",
"gameWithPassword": "لعبة مع كلمة السر",
"tutorialQuestion": "هل تريد أن تتعلم العزف مع جولة البرنامج التعليمي؟",
"FORUM": "FORUM",
"androidAppQuestion": "فتح مع الروبوت التطبيق؟",
"restartGameQuestion": "إعادة تشغيل اللعبة؟",
"playGameQuestion": "هل تريد الانضمام إلى هذه اللعبة؟ <br/> (أنت الآن متفرج)",
"restartGame": "إعادة تشغيل اللعبة",
"startGame": "بدء اللعبة",
"nightSelect": "ليلة حدد",
"cannotEmpty": "لا يمكن فارغة!",
"tutorialDone": "البرنامج التعليمي القيام به",
"gameNameAlreadyExists": "اسم اللعبة موجود بالفعل",
"spectatorMode": "انقر للانضمام (المتفرج)",
"gameNotPublic": "لعبة ليست عامة",
"isBot": "هو بوت",
"friendAdded": "وأضاف صديق",
"OrCopyLink": "..or نسخ ومشاركة هذا الرابط:",
"cantPlaySpectator": "لا يمكنك الانضمام إلى اللعبة إذا بدأت بالفعل!",
"gameRestartForPlayersChange": "تم إعادة تشغيل اللعبة لأن عدد اللاعبين قد تغير",
"someSpectator": "المشاهدين الحالي اللاعبين لن تلعب اللعبة. <br> إذا كنت تريد من الجميع تشغيلها، فيجب عليهم النقر على اسمهم للانضمام!",
"clipboardCopy": "نسخ إلى الحافظة",
"shareThisError": "واسمحوا لنا أن نعرف هذا الخطأ على صفحة 'smltown.tk'، وذلك بفضل.",
"lookingGames": "تبحث عن ألعاب ..",
});
