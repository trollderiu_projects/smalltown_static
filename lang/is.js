$.extend(window.lang, {

"startingConnection": "Upphaf tenging ..",
"joiningGame": "Ganga í leikinn ...",
"GameList": "Leikur Listi:",
"noMoreGames": "Ekki fleiri leiki",
"emptyChat": "Vertu fyrstur til að skrifa eitthvað í spjallinu!",
"createGame": "Búa til leik",
"gameName": "Leikur nafn",
"hideGameSwipe": "Strjúka til að fela þennan leik ..",
"exitGameSwipe": "Þú verður að hætta frá þessum leik ..",
"removeGameSwipe": "Strjúktu til að fjarlægja leikinn varanlega ..",
"playingHere": "Þú ert að spila hér",
"playersIn": " Virkir leikmenn í",
"games": " leikir.",
"Admin": "Admin",
"adminHelp": "Admin aðgerðir",
"NewCards": "Nýtt kort",
"newCardsHelp": "(Segðu öllum að horfa á spilakort)",
"StartGame": "Byrja leik",
"startGameHelp": "(Segðu allir loka augu)",
"gameHelp": "Leik ADMIN valkosti",
"EndTurn": "Loka beygja",
"endTurnHelp": "Lokadaginn snúa",
"Game": "Leikur",
"Password": "Lykilorð",
"GamePublic": "Leikurinn er opinberur",
"DayTime": "Daginn eftir leikmann",
"OpenVoting": "Láttu leikmenn kjósa á daginn",
"AdminEndTurn": "Admin getur lokað dagaskiptingu",
"sec_p": "Sek / p",
"PlayingCards": "Spil",
"cardsHelp": "Kortalisti",
"Name": "Nafn",
"UserSettings": "Notendastillingar",
"userHelp": "Persónulegar valkostir",
"SpectatorMode": "Spectator ham",
"spectatorModeHelp": "Vera áhorfandi til að spjalla án þess að spila",
"Image": "Mynd",
"VibrationStrength": "Titringur",
"CleanErrors": "Hreinn Villa",
"cleanHelp": "Einhver vandamál? Láttu okkur vita á 'smltown.tk'",
"Friends": "Vinir",
"friendsHelp": "Finndu vini þína",
"Invite": "Bjóddu",
"inviteHelp": "Bjóddu vistuðum vinum",
"AddSocialId": "Bæta við auðkenni",
"EditSocialId": "Breyta auðkenni þínu",
"addSocialIdHelp": "Bætið auðkenni þínu til að verða að finna",
"Info": "Upplýsingar",
"infoHelp": "Hjálp og leik handbók",
"CurrentURL": "Núverandi slóð",
"Notes": "Skýringar",
"infoNotes": "Vista athugasemdir við næstu snýr leiksins",
"Back": "Fara aftur",
"backHelp": "Aftur til leiklist",
"newCards": "Takast á við nýtt spilakort",
"waitingNewGame": "Bíða eftir nýjum leik ...",
"gameRestarted": "Leikurinn var endurræstur",
"noName": "Ekkert nafn fannst",
"waitPlayersVotes": "Bíða leikmenn atkvæði",
"nightTime": "Nótt",
"dayTime": "Daginn",
"wakingUp": "Bíða leikmenn vakna ..",
"waiting": "Bíður að spila",
"spectatorPlayer": "Áhorfandi",
"alive": "Lifandi",
"dead": "Dauður",
"morePlayers": "Hringdu í fleiri leikmenn",
"morePlayersHelp": "Lágmark 4 leikmenn til að spila",
"PRESELECT": "Smelltu aftur til að velja",
"Win": "VINNAR TEAM",
"OK": "Allt í lagi",
"Cancel": "Hætta við",
"NoKills": "Þorpið hefur ekki náð samkomulagi og enginn hefur verið lynched",
"NoKillsTonight": "Allir vakna án dauða.",
"wasKilled": "Hefur verið lynched",
"GettingDark": "Er að verða dökk!",
"closeEyes": "Lokaðu augunum",
"wakeUp": "Vaknaðu",
"yourTurn": "Er að snúa þér",
"GoodMorning": "Góðan daginn! Það er kominn tími til að ræða hver við ættum að losa! <P> (Þegar sólin nær langt til hægri er tíminn liðinn) </ p>",
"gameWillStart": "Leikurinn mun byrja fljótlega, lokaðu augunum!",
"alive2": "Aðeins 2 leikmenn eru á lífi. Engin dráp gæti gerst á dag",
"wasKilledTonight": "Hefur dáið í kvöld",
"dayEnd": "Nótt er að koma. Allir loka augunum.",
"GameOver": "LEIK LOKIÐ",
"botsAdded": "Það eru ekki nóg leikmenn, sumir bots hafa verið bætt við. <P> Leikurinn mun byrja fljótlega </ p>",
"lynch": "Veldu einn leikmann fyrir lynching! <P> (Tal er nú bannað) </ p>",
"warnServer": "Varið: Miðlarinn tekur of langan tíma. Það getur verið vefþjónsvilla, bíddu lítið eða hressaðu leikinn.",
"openVotingEnabled": "Fyrir næsta leik verður atkvæðagreiðsla leyfður á daginn",
"openVotingDisabled": "Fyrir næsta leik verður ekki kosið atkvæði á daginn",
"adminEndTurnEnabled": "Fyrir næsta leik, getur húsbóndi lýkur dagaskiptingu",
"adminEndTurnDisabled": "Fyrir næsta leik, getur húsbóndi ekki lokið dagaskiptingu",
"updatedDayTime": "Fyrir næsta leik hefur lengd dagsins verið breytt",
"passwordRemoved": "Lykilorð leikur var fjarlægður",
"passwordUpdated": "Uppfært lykilorð leik",
"adminRole": "Þú stal admin hlutverk!",
"noCard": "Ekkert kort ennþá",
"awakening": "Bíddu! Sumir leikmenn eru vakandi!",
"youSpectator": "Þú ert áhorfandi",
"youDead": "Hey! Þú ert dauður",
"somethingHappend": "Einhver er að íhuga eitthvað ...",
"noCardsSelected": "Vinsamlegast veldu spilakort",
"duplicatedUserName": "Nafn er þegar til!",
"cannotCardsGame": "Ekki er hægt að skipta um spil í leik",
"cannotCardsAdmin": "Aðeins admin getur skipt um leik spil",
"InviteFriends": "Bjóða vinum",
"Update": "Uppfæra",
"addFriend": "Bæta við vini",
"SendInvitation": "Senda boð",
"demoUser": "Demo notandi",
"winner": "Þú vann leikinn!",
"Share": "Deila!",
"ShareBy": "Deila með",
"help_typeNameGame": "Sláðu inn nafn leik með lágmarki <b> 3 </ b> stafi og er ekki til.",
"help_createGame": "Smelltu á 'búa til leik'",
"error_tutorialPlayers": "Ekki er hægt að hefja kennslu í leik með leikmönnum",
"reload": "Endurhlaða",
"endingDay": "Endir dagur",
"nameMustLength": "Nafnið verður að vera með 3 stafir að lágmarki",
"adminCanEndTurn": "Admin getur lokað beygja strax",
"backAgain": "Aftur til að hætta",
"addUserInGame": "Bæta við notanda í leik",
"gamePublic": "Opinber leikur",
"helpStop": "Stöðva einkatími",
"playGame": "spila leik",
"setName": "Stilla nafn",
"savedName": "Vistað nafn",
"cannotModifyCardsInGame": "Ekki er hægt að breyta spilum í leik",
"gameNameExists": "Leikur nafn er til",
"letVoteDay": "Láta leikmenn kjósa á daginn",
"timeByPlayer": "Sekúndur dagsins eftir leikmanni",
"gameWithPassword": "Leikur með lykilorði",
"tutorialQuestion": "Viltu læra að spila með námskeiði?",
"FORUM": "FORUM",
"androidAppQuestion": "Opna með Android app?",
"restartGameQuestion": "Endurræsa leik?",
"playGameQuestion": "Viltu taka þátt í þessum leik? <br/> (þú ert nú áhorfandi)",
"restartGame": "Endurræsa leik",
"startGame": "Hefja leik",
"nightSelect": "Næturvalið",
"cannotEmpty": "Getur ekki tæmt!",
"tutorialDone": "Kennsla lokið",
"gameNameAlreadyExists": "Leikur heiti er þegar til",
"spectatorMode": "Smelltu til að taka þátt (áhorfandi)",
"gameNotPublic": "Leikur ekki opinber",
"isBot": "Er látinn",
"friendAdded": "Vinur bætt við",
"OrCopyLink": ".. eða afritaðu og deildu þessum tengil:",
"cantPlaySpectator": "Þú getur ekki tekið þátt í leik ef það er þegar hafin!",
"gameRestartForPlayersChange": "Leikurinn hefur verið endurræstur vegna þess að leikmaðurarnúmerið hefur breyst",
"someSpectator": "Núverandi áhorfendur munu ekki spila leikinn. <br> Ef þú vilt að allir þeirra spili, verða þeir að smella á nafnið sitt til að taka þátt!",
"clipboardCopy": "Afritað á klemmuspjald",
"shareThisError": "Vinsamlegast láttu okkur vita þessa villa á vefsíðu 'smltown.tk', takk.",
"lookingGames": "Leita að leikjum ..",
});
