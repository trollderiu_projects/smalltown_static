$.extend(window.lang, {

"startingConnection": "Conectare inițială ..",
"joiningGame": "Aderarea la joc ...",
"GameList": "Lista de jocuri:",
"noMoreGames": "Nu mai multe jocuri",
"emptyChat": "Fii primul care scrie ceva în chat!",
"createGame": "a crea un joc",
"gameName": "Numele jocului",
"hideGameSwipe": "Glisați pentru a ascunde acest joc ..",
"exitGameSwipe": "Veți ieși din acest joc ..",
"removeGameSwipe": "Glisați pentru a elimina permanent jocul ..",
"playingHere": "Joci aici",
"playersIn": " Jucători activi în",
"games": " jocuri.",
"Admin": "admin",
"adminHelp": "Acțiuni de administrare",
"NewCards": "Carduri noi",
"newCardsHelp": "(Spune tuturor să-și uite cărțile de joc)",
"StartGame": "Incepe jocul",
"startGameHelp": "(Spun toti ochii aproape)",
"gameHelp": "Opțiuni de joc ADMIN",
"EndTurn": "Sfârșitul virajului",
"endTurnHelp": "Sfârșitul zilei de sfârșit",
"Game": "Joc",
"Password": "Parola",
"GamePublic": "Jocul este public",
"DayTime": "Timp de zi de jucator",
"OpenVoting": "Lăsați jucătorii să voteze în timpul zilei",
"AdminEndTurn": "Adminul poate termina schimbarea zilei",
"sec_p": "sec / p",
"PlayingCards": "Carti de joc",
"cardsHelp": "Lista de carduri",
"Name": "Nume",
"UserSettings": "Setarile utilizatorului",
"userHelp": "Opțiuni personale",
"SpectatorMode": "Modul Spectator",
"spectatorModeHelp": "Fi spectator la chat fără a juca",
"Image": "Imagine",
"VibrationStrength": "Viteza vibrațiilor",
"CleanErrors": "Curățați erorile",
"cleanHelp": "Orice problemă? Anunțați-ne pe pagina 'smltown.tk'",
"Friends": "Prieteni",
"friendsHelp": "gaseste-ti prietenii",
"Invite": "A invita",
"inviteHelp": "Invitați prieteni salvați",
"AddSocialId": "Adăugați identificator",
"EditSocialId": "Editează-ți identificatorul",
"addSocialIdHelp": "Adăugați-vă identitatea pentru a fi găsită",
"Info": "Info",
"infoHelp": "Ajutor și manual de joc",
"CurrentURL": "URL-ul curent",
"Notes": "notițe",
"infoNotes": "Salvați notele la viitoarele rotiri ale jocului",
"Back": "Întoarcere",
"backHelp": "Înapoi la lista de jocuri",
"newCards": "Rezolva cărți noi",
"waitingNewGame": "Așteptând un nou joc ...",
"gameRestarted": "Jocul a fost repornit",
"noName": "Nici un nume nu a fost găsit",
"waitPlayersVotes": "Voturile jucătorilor așteptați",
"nightTime": "Noaptea",
"dayTime": "Zi",
"wakingUp": "Jucătorii de așteptare trezesc ..",
"waiting": "Așteptând să se joace",
"spectatorPlayer": "spectator",
"alive": "în viaţă",
"dead": "mort",
"morePlayers": "Apelați mai mulți jucători",
"morePlayersHelp": "Minim 4 jucători pentru a juca",
"PRESELECT": "Dați din nou clic pentru a selecta",
"Win": "ECHIPA DE WINNER",
"OK": "O.K",
"Cancel": "Anulare",
"NoKills": "Satul nu a ajuns la un acord și nimeni nu a fost lins",
"NoKillsTonight": "Toată lumea se trezește fără moarte.",
"wasKilled": "A fost lins",
"GettingDark": "Se întunecă!",
"closeEyes": "inchide ochii",
"wakeUp": "trezeşte-te",
"yourTurn": "Este rândul tău",
"GoodMorning": "Buna dimineata! Este timpul să discutăm cine ar trebui să lingăm! <P> (Când soarele ajunge la extrema dreaptă, timpul va fi terminat) </ p>",
"gameWillStart": "Jocul va începe în curând, închideți-vă ochii!",
"alive2": "Doar 2 jucători trăiesc. Nu s-ar putea întâmpla nimic în acea zi",
"wasKilledTonight": "A murit diseară",
"dayEnd": "Noaptea vine. Toți închideți ochii.",
"GameOver": "JOC ÎNCHEIAT",
"botsAdded": "Nu există jucători capabili, s-au adăugat niște roboți. <P> Jocul va începe în curând </ p>",
"lynch": "Selectați un jucător pentru linsing! <P> (vorbiți acum este interzisă) </ p>",
"warnServer": "Avertizați: serverul durează prea mult. Poate fi o eroare de server, așteptați puțin sau reîmprospătați jocul.",
"openVotingEnabled": "Pentru următorul joc, votarea va fi permisă în timpul zilei",
"openVotingDisabled": "Pentru următorul joc, votarea nu va fi permisă în timpul zilei",
"adminEndTurnEnabled": "Pentru următorul joc, comandantul poate termina schimbarea zilei",
"adminEndTurnDisabled": "Pentru următorul joc, comandantul nu poate termina schimbarea zilei",
"updatedDayTime": "Pentru următorul joc, durata zilei a fost modificată",
"passwordRemoved": "Jocul parolă a fost eliminat",
"passwordUpdated": "Actualizat parola joc",
"adminRole": "Ai furat rolul de administrator!",
"noCard": "Niciun card încă",
"awakening": "Stai !, unii jucători se trezesc!",
"youSpectator": "Esti un spectator",
"youDead": "Hei !, ești mort",
"somethingHappend": "Cineva tratează ceva ...",
"noCardsSelected": "Selectați cărțile de joc",
"duplicatedUserName": "Numele există deja!",
"cannotCardsGame": "Nu pot schimba cardurile în joc",
"cannotCardsAdmin": "Numai adminul poate schimba cărțile de joc",
"InviteFriends": "Invita prieteni",
"Update": "Actualizați",
"addFriend": "adauga prieten",
"SendInvitation": "Trimite invitatia",
"demoUser": "Utilizator demo",
"winner": "Ai câștigat jocul!",
"Share": "Acțiune!",
"ShareBy": "Trimiteți prin",
"help_typeNameGame": "Introduceți un nume de joc cu un număr minim de <b> 3 </ b> și încă nu există.",
"help_createGame": "Faceți clic pe 'creați joc'",
"error_tutorialPlayers": "Nu se poate începe tutorialul în joc cu jucătorii",
"reload": "reincarca",
"endingDay": "Sfârșitul zilei",
"nameMustLength": "Numele trebuie să aibă o lungime minimă de 3 caractere",
"adminCanEndTurn": "Adminul se poate termina imediat",
"backAgain": "Înapoi pentru a ieși",
"addUserInGame": "Adăugați utilizator în joc",
"gamePublic": "Joc public",
"helpStop": "Opri tutorial",
"playGame": "joaca jocul",
"setName": "Set nume",
"savedName": "Nume salvat",
"cannotModifyCardsInGame": "Nu pot modifica cardurile în joc",
"gameNameExists": "Numele jocului există",
"letVoteDay": "Lăsați jucătorii să voteze în timpul zilei",
"timeByPlayer": "Secunde ale zilei de către jucător",
"gameWithPassword": "Joc cu parola",
"tutorialQuestion": "Vrei să înveți să te joci cu un tur tutorial?",
"FORUM": "FORUM",
"androidAppQuestion": "Deschideți cu aplicația Android?",
"restartGameQuestion": "Reporniți jocul?",
"playGameQuestion": "Vrei să te înscrii în acest joc? <br/> (acum sunteți un spectator)",
"restartGame": "Reporniți jocul",
"startGame": "incepe jocul",
"nightSelect": "Alegeți noapte",
"cannotEmpty": "Nu poate fi gol!",
"tutorialDone": "Tutorial făcut",
"gameNameAlreadyExists": "Numele jocului există deja",
"spectatorMode": "Faceți clic pe pentru a vă alătura (spectator)",
"gameNotPublic": "Joc nu public",
"isBot": "Este bot",
"friendAdded": "Prietenul a adăugat",
"OrCopyLink": "..au copiați și distribuiți acest link:",
"cantPlaySpectator": "Nu te poți alătura la joc dacă a început deja!",
"gameRestartForPlayersChange": "Jocul a fost repornit deoarece numărul jucătorilor sa schimbat",
"someSpectator": "Jucătorii actuali de spectatori nu vor juca acest joc. <br> Dacă doriți ca toți să joace, trebuie să faceți clic pe numele lor pentru a vă alătura!",
"clipboardCopy": "Copiat în clipboard",
"shareThisError": "Spuneți-ne această eroare pe pagina web 'smltown.tk', mulțumită.",
"lookingGames": "Caută jocuri ..",
});
